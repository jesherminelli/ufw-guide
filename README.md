## Principais comandos do ufw ====> iptables

- **Instalação**

```
sudo apt install ufw
man ufw
```

- **verificar atividade**

```
ufw status
```
- **negar todo tipo de entrada, em qualquer porta. isso da uma boa segurança para usuário comum**

```
ufw default deny incoming
```

- **Negar todo tipo de saída**

```
ufw default deny outgoing

```

- **Ativa-lo no boot**

```
ufw enable
systemctl status ufw
systemctl start ufw
systemctl enable ufw

```

- **Habilitar algumas portas para fora**

```
ufw allow out 80 - http protocol
ufw allow out 443 - https protocol
ufw allow out 53 - dns protocol
ufw status
```

- **Recarregar serviço**

```
ufw reload

```

- **Liberar a entrada da porta ssh**

```
ufw allow 2376 - porta ssh - obs: "NÃO UTILIZE SSH NA PORTA 22"
```

- **Listar numeradamente**

```
ufw status numbered
```

- **Deletar por número**

```
ufw delete 1
ufw delete 3

```

- **Resetar todas regras do ufw**

```
ufw reset
```

- **Faço todo o passo novamente**

```
ufw status
ufw enable
```

- **listar todos apps disponiveis**

```
ufw app list
```

- **Negar entrada e saída de um endereço específico**

```
ufw deny from 192.168.1.101

```

- **Negar apenas entrada de um endereço específico**

```
ufw deny in from 192.168.1.101
```

- **deixar passar ip até a faixa 24**

```
ufw allow from 192.168.1.101/24 to any port 2376
```